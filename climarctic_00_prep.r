#%#%#
# climarctic preparation
#%#%#

print('##### Climarctic 00 data prep #####')

rm(list=ls())

require(foreach)
require(doSNOW) # e.g. makeSOCKcluster()
require(RColorBrewer) # brewer.pal
require(zCompositions)
require(caret)
require(xtable)

# prep cluster
cl <- makeSOCKcluster(4)

clusterEvalQ(cl, library(zCompositions))

registerDoSNOW(cl)

#---
restart <- F

lst_plot <- NULL

depths <- c('top|deep','top','deep')

n_comm <- c('01_16S_bact','02_18S_euk','05_ITS_fun')

swt <- function(x){
  switch(x,
         '01_16S_bact'='Bacteria',
         '02_18S_euk'='Eukaryota',
         '05_ITS_fun'='Fungi')
}

v_selec <- c('site','moisture','depth','rh','pH','C','N','C_N','silt','clay','om')


# DIR LOAD ####

dir_in <- 'Projets/Climarctic/stats/MBC/in/'
dir_out <- 'Projets/Climarctic/stats/MBC/out/'
dir_prep <- paste0(dir_out, '00_prep/')
dir_save <- paste0(dir_out, 'saves/')
dir.create(dir_prep, showWarnings=F, recursive=T)
dir.create(dir_save, showWarnings=F, recursive=T)


# GRAPHICAL PARAMETERS ####

# figure widths
wdt1 <- 3.35
wdt2 <- 6.89


# ENV ####
env_ini_fact <- read.table(paste0(dir_in, 'env_clim_grad.csv'), row.names=1, h=T)
env_ini_chim <- read.table(paste0(dir_in, 'env_clim_chim.csv'), h=T)

# reorder the two matrices ---

# env fact
rn <- row.names(env_ini_fact)
row.names(env_ini_fact) <- ifelse(as.numeric(rn) < 100, ifelse(as.numeric(rn) < 10, paste0('T00', rn), 
                                                               paste0('T0', rn)), paste0('T', rn))

# env_chim
env_ini_chim <- env_ini_chim[c(matrix(1:36, nrow=2, byrow=T)),]
env_ini_chim <- env_ini_chim[as.numeric(gl(36,3)),]

# reorganise the total environmental matrix ---

# take the interesting variables
env_ini_tot <- cbind.data.frame(env_ini_fact[,c(2:5,7:13,15)], env_ini_chim[,c('sand','silt','clay')])
names(env_ini_tot)[1:9] <- c('site','moisture','plot','depth','quadrat','empty','fresh','dry','burn')

# calculate relative humidity, organic matter content and C/N ratio
env_ini_tot[,c('fresh','dry','burn')] <- sapply(env_ini_tot[,c('fresh','dry','burn')], function(x) x-env_ini_tot$empty)
env_ini_tot$rh <- (env_ini_tot$fresh-env_ini_tot$dry) / env_ini_tot$fresh
env_ini_tot$om <- (env_ini_tot$dry-env_ini_tot$burn) / env_ini_tot$dry
env_ini_tot$C_N <- env_ini_tot$C / env_ini_tot$N

env_ini_tot <- env_ini_tot[,-grep(paste(c('empty','fresh','dry','burn'), collapse='|'), names(env_ini_tot))]

# relabel a few factors
env_ini_tot$site <- gl(2, 54, labels=c('Knudsenheia','Ossian'))
env_ini_tot$moisture <- ordered(gl(3,18,108, c('dry','intermediate','wet')))
env_ini_tot$plot <- as.factor(env_ini_tot$plot)
env_ini_tot$depth <- rep(gl(2,3, labels=c('top','deep')), 18)
env_ini_tot$depth <- factor(env_ini_tot$depth, levels=c('top','deep'))

env_ini_tot$MiS <- factor(apply(env_ini_tot[,c('site','moisture')], 1, function(x) paste(x, collapse='_')))
env_ini_tot$combi <- factor(paste(env_ini_tot$MiS, env_ini_tot$depth, sep='_'))

# remove the outliar pH
env_ini_tot$pH[env_ini_tot$pH > 8.5] <- NA

# model the missing values
env_tot <- predict(preProcess(env_ini_tot, method='knnImpute'), newdata=env_ini_tot)

# check which variable is numeric
is.num <- sapply(env_tot, is.numeric)

# unscale the env_tot for the MRT
env_unsc <- env_ini_tot[,names(env_tot)]
env_unsc[,is.num] <- sapply(names(env_unsc)[is.num], function(x){
  knn <- env_tot[[x]]
  ini <- env_ini_tot[[x]]
  
  knnNA <- ifelse(is.na(ini), NA, knn)
  
  rngknn <- range(knnNA, na.rm=T)
  rngini <- range(ini, na.rm=T)
  
  knn_sc <- (knn-rngknn[1])/diff(rngknn)
  
  return(knn_sc*diff(rngini)+rngini[1])
  
})


# palette env ----
lst_palev <- list(site    =data.frame(col=c(1,1),   pch=c(22,21),  bg=c(1,1)),   
                  moisture=data.frame(col=rep(0,3), pch=rep(21,3), bg=brewer.pal(8, 'Set1')[c(7,5,2)]),
                  depth   =data.frame(col=c(1,1),   pch=c(21,21),  bg=c(1,0)))

lst_palev <- lapply(lst_palev, function(x) sapply(x, function(y) as.character(y)))

row.names(lst_palev$site)      <- levels(env_tot$site)
row.names(lst_palev$moisture) <- levels(env_tot$moisture)
row.names(lst_palev$depth)    <- levels(env_tot$depth)


# plot functions ----

# legend function
leg <- function(x = 0.5, y = 0.5, add = F, title=NULL, col=NULL, pch=NULL, bg=NULL, lty=NULL){
  
  if(add == F){
    plot.new()
    x <- 0.5
    y <- 0.5
  }
  
  pchl <- as.numeric(unlist(sapply(lst_palev, function(x) x[,'pch'])))
  coll <- unlist(sapply(lst_palev, function(x) x[,'col']))
  bgl  <- unlist(sapply(lst_palev, function(x) x[,'bg']))
  
  if(is.null(pch) == F){
    pchl <- pch
  }
  if(is.null(col) == F){
    coll <- col
  }
  if(is.null(bg) == F){
    bgl <- bg
  }
  
  legend(x,y, legend=unlist(sapply(lst_palev, row.names)), bty='n', xjust=0.5, yjust=0.5,
         pch=pchl, col=coll, pt.bg=bgl, title=title, lty=lty)
}

# layout function
lay_ordi <- function(x, x2=NULL, env=env_tot) {
  
  # layout
  par(mfrow=c(2,2), mar=c(5,4,4,4))
  
  # loop the depths
  for(i in depths){
    
    # extraction of coordinates
    if(is.null(x2)){
      smp <- x[[i]]$smp
      var <- x[[i]]$var
      axes_names <- x[[i]]$axes_names
    } else {
      smp <- x[[i]][[x2]]$smp
      var <- x[[i]][[x2]]$var
      axes_names <- x[[i]][[x2]]$axes_names
    }
    
    e <- env[row.names(smp),]
    
    # calculation of the var axes
    rng_smp <- sapply(as.data.frame(smp), range)
    rng_var <- sapply(as.data.frame(var), range)
    
    divis <- max(rng_var/rng_smp)
    v2 <- var/divis
    
    max_smpx <- max(abs(rng_var[,1]/divis))
    max_varx <- max(abs(rng_var[,1]))
    
    max_smpy <- max(abs(rng_var[,2]/divis))
    max_vary <- max(abs(rng_var[,2]))
    
    at_varx <- seq(-max_smpx, max_smpx, length.out=9)
    at_vary <- seq(-max_smpy, max_smpy, length.out=9)
    
    # plot ---
    plot(rng_smp[,1], rng_smp[,2], type='n', xlab=axes_names[1], ylab=axes_names[2])
    
    abline(v=0, h=0, lty=3)

    # axes
    axis(3, at=at_varx, labels=round(at_varx, 2), col=2)
    axis(4, at=at_vary, labels=round(at_vary, 2), col=2)
    
    # arrow and spiders
    ordispider(smp, e$combi, col='grey80')
    
    arrows(0,0,v2[,1],v2[,2], length=0, lty=2, col='grey60')
    
    # variables and samples
    points(smp, pch=as.numeric(lst_palev$site[e$site,'pch']), col=lst_palev$moisture[e$moisture,'bg'],
           bg=ifelse(as.numeric(lst_palev$depth[e$depth,'bg']), lst_palev$moisture[e$moisture,'bg'], 0))
    
    text(v2[,1:2], labels=paste(row.names(v2)))
    
  }
  
  # legend
  leg()
  
}


# PCA ----

# check which of the granulometry fraction to remove based on the PCA loading
for(i in depths){
  
  ind_depth <- grep(i, env_tot$depth)
  
  graname <- c('sand','silt','clay') 
  
  for(j in graname){
    e <- env_tot[ind_depth,is.num & names(env_tot) != j]
    
    pca <- rda(e)
    load <- pca$CA$v
    
    print(c(i, j, round(sum(rowSums(abs(load[row.names(load) %in% graname,]))), 2)))
    
  }
}
# according to the results, for all the depth combination, the sand should be removed


# pca analyse and grafs ----

lst_pca <- NULL
for(i in depths){
  
  # prepare the data
  ind_depth <- grep(i, env_tot$depth)
  e <- env_tot[ind_depth,]
  
  #---
  pca <- rda(e[is.num & names(env_tot) != 'sand'])
  
  # retreive the coordinates
  smp <- pca$CA$u[,1:2]
  var <- pca$CA$v[,1:2]
  
  eig <- pca$CA$eig
  eig <- round((eig/sum(eig)), 2)[1:2]
  
  axes_names <- paste0('PCA', 1:2, '; var = ', eig)
  
  #---
  lst_pca[[i]] <- list(smp=smp, var=var, axes_names=axes_names, pca=pca)

}

# plot PCA
X11(width=wdt2, height=wdt2*1.2)
lay_ordi(lst_pca)

lst_plot[['PCA']] <- recordPlot()
dev.off()


# COMMUNITIES ####

# loop the primers
lst_comm <- NULL
for(i in n_comm) {
  
  # initialize ----
  print(paste('####',i,'####'))
  
  id_plate <- substr(i, 1, 2)

  #---
  file <- paste0(dir_save, '00_ini_', id_plate, '.Rdata')
  if(file.exists(file) & restart == F){
    load(file)
  } else {
    mr_ini  <- read.table(paste0(dir_in, 'from_cluster/', id_plate, '/', id_plate, '_clust.mr'), h=T)
    fa_ini  <- read.table(paste0(dir_in, 'from_cluster/', id_plate, '/', id_plate, '_clust.fa'))
    lst_ass_ini <- lapply(list.files(paste0(dir_in, 'from_cluster/', id_plate), '.ass', full.names=T), read.table)
    save(mr_ini, lst_ass_ini, fa_ini, file=file)
  }

  
  # reorganize mr (clean blanks) ----
  
  mr_tot <- mr_ini[grep('T|B', row.names(mr_ini)),]

  # remove the OTU found in the blanks
  ind_blk <- grepl('B', row.names(mr_tot))

  print(c('smps', nrow(mr_tot[ind_blk == F,]), 'OTUs', ncol(mr_tot[ind_blk == F,colSums(mr_tot[ind_blk == F,]) != 0]),
          'reads', sum(mr_tot[ind_blk == F,])))

  # plot to see how much sequences have the blanks
  # cairo_ps(paste0(dir_prep, 'blanks_', i, '.eps'))
  
  rs <- rowSums(mr_tot)
  ord <- order(rs)
  
  X11(width=wdt1, height=wdt1)
  plot(rs[ord], col=as.numeric(ind_blk[ord])+1, pch=19, main=i, ylab='nb seq')

  lst_plot[['blk']][[i]] <- recordPlot()
  dev.off()
  
  # find the blank OTUs and remove them 
  if(length(which(ind_blk))){
    ind_conta <- colSums(decostand(mr_tot[ind_blk,], 'pa')) != 0
  } else {
    ind_conta <- rep(F, ncol(mr_tot))
  }

  mr_tot <- mr_tot[grep('T', row.names(mr_tot)),ind_conta == F]
  mr_tot <- mr_tot[rowSums(mr_tot) != 0,colSums(mr_tot) != 0]

  
  # reorganize ass ----
  # reorganize fa
  n_fa <- as.character(fa_ini[seq(1,nrow(fa_ini), by=2),])
  n_fa <- substr(n_fa, 2, nchar(n_fa))

  fa_tot <- fa_ini[seq(2,nrow(fa_ini), by=2),]
  names(fa_tot) <- n_fa

  fa_tot <- fa_tot[names(mr_tot)]

  # reorganize ass
  lst_ass_tot <- NULL
  for(j in seq_along(lst_ass_ini)){
    ass_ini <- lst_ass_ini[[j]]

    tax <- ass_ini$V2
    names(tax) <- ass_ini$V1
    tax <- tax[names(mr_tot)]

    ass_tot <- data.frame(taxo=tax, seq=fa_tot)

    ass_tot$taxo <- gsub('[[:punct:]][[:digit:]]{2,3}[[:punct:]]{2}|;', '|', ass_tot$taxo)

    # correct the taxon that are found at many tax lev
    ass_tot$taxo <- gsub('Candidatus_Magasanikbacteria|Parcubacteria', 
                         'Candidatus_Magasanikbacteria|Candidatus_Magasanikbacteria_unclassified', ass_tot$taxo, fixed=T)
    ass_tot$taxo <- gsub('Elusimicrobia|Lineage_IIa', 'Elusimicrobia|u', ass_tot$taxo, fixed=T)
    ass_tot$taxo <- gsub('_|', '|', ass_tot$taxo, fixed=T)
    ass_tot$taxo <- gsub('__', '_', ass_tot$taxo, fixed=T)

    # correct taxon with different parent taxon
    ass_tot <- ass_tot[names(mr_tot),]

    
    # taxo make ----
    taxo_tot <- strsplit(as.character(ass_tot$taxo), '|' , fixed=T)
    nb_lev <- length(taxo_tot[[1]])

    taxo_tot <- matrix(unlist(taxo_tot), ncol=nb_lev, byrow=T)
    row.names(taxo_tot) <- row.names(ass_tot)

    taxo_tot <- as.data.frame(t(parApply(cl, taxo_tot, 1, function(x) {

      # scrap taxonomy correction
      ind_scrap <- which(x %in% 'Geobacter_sp.'
                         | x %in% 'Acidobacteria'
                         | x %in% 'Acidobacteria_bacterium'
                         | x %in% 'Rhodospirillales_bacterium'
                         | x %in% 'Actinobacteria_bacterium'
                         | x %in% 'Sterolibacterium'
                         | x %in% 'Sphingosinicella_sp.'
                         | x %in% 'Sphingomonas_sp.'
                         | x %in% 'Rhodoferax_sp.'
                         | x %in% 'Rhodococcus_sp.'
                         | x %in% 'Rhodobacter_sp.'
                         | x %in% 'Pseudanabaena'
                         | x %in% 'Nocardioides_sp.'
                         | x %in% 'Microbacterium_sp.'
                         | x %in% 'Mesorhizobium_sp.'
                         | x %in% 'Leptothrix_sp.'
                         | x %in% 'Kribbella_sp.'
                         | x %in% 'Devosia_sp.'
                         | x %in% 'Cellulomonas_sp.'
                         | x %in% 'Caulobacter_sp.'
                         | x %in% 'Caenimonas_sp.'
                         | x %in% 'Betaproteobacteria_bacterium'
                         | x %in% 'Bacteroidetes_bacterium'
                         | x %in% 'Afipia_sp.'
                         | x %in% 'Afipia_genosp.'
                         | x %in% 'Xanthomonadaceae_bacterium'
                         | x %in% 'Sphingobacteriaceae_bacterium'
                         | x %in% 'Rhodospirillaceae_bacterium'
                         | x %in% 'Monodopsis_sp.'
                         | x %in% 'Cytophagaceae_bacterium'
                         | x %in% 'Caulobacteraceae_bacterium'
                         | x %in% 'Candidatus'
                         | x %in% 'Sphingobacteriales_bacterium'
                         | x %in% 'Bdellovibrionales_bacterium'
                         | x %in% 'Burkholderiales_bacterium'
                         | x %in% 'Hypsibius_dujardini'
                         | x %in% 'Nostoc'
                         | x %in% 'Verrucomicrobia'
                         | x %in% 'Synechococcus'
                         | x %in% 'Sphingobacterium'
                         | x %in% 'Sorangiineae'
                         | x %in% 'Nitrospirae'
                         | x %in% 'Armatimonadetes_bacterium'
                         | x %in% 'Antarctic'
                         | x %in% 'Clostridiales_bacterium'
      )[1]
      
      if(is.na(ind_scrap) == F) x[ind_scrap:length(x)] <- 'u'

      x <- gsub('.*Incertae_.*|Unknown|.*-[pcofgs]$|.*_unclassified', 'u', x)
      x <- gsub('_Incertae|_clade|_lineage|_terrestrial|_marine|_genosp.|_sensu|_sp.|_bacterium','', x)

      # set as unknown the scraps taxonomies 
      for(j in 2:length(x)){
        if(x[j] %in% x[1:(j-1)]){
          x[j] <- 'u'
        }
      }

      ind_unc <- grep('^[[:lower:]]', x)
      if(length(ind_unc)){
        if(ind_unc[1] == 1){
          x[1] <- 'Life'
          ind_unc <- ind_unc[-1]
        }
      }
      
      for(j in ind_unc){
        x[j] <- ifelse(grepl('_X', x[j-1]), paste0(x[j-1], 'X'), paste0(x[j-1], '_X'))
      }
      
      return(x)
      
    })))

    lst_ass_tot[[j]] <- list(ass_tot=ass_tot, taxo_tot=taxo_tot)
  }

  # rename OTU according to dataset
  id <- switch(i,
               '01_16S_bact' = 'bac',
               '02_18S_euk' = 'euk',
               '05_ITS_fun' = 'fun')
  
  names(mr_tot) <- sub('X_', paste0(id, '_'), names(mr_tot))
  
  lst_ass_tot <- lapply(lst_ass_tot, function(x) lapply(x, function(y){
    z <- y
    row.names(z) <- names(mr_tot)
    return(z)
  }))

  # sort taxo
  ass_tot <- lst_ass_tot[[1]]$ass_tot
  taxo_tot <- lst_ass_tot[[1]]$taxo_tot

  # improve the taxo with blastn best match of the OTUs against Unite
  if(i == '05_ITS_fun'){
    
    tt1 <- as.matrix(lst_ass_tot[[1]]$taxo_tot)
    tt2 <- as.matrix(lst_ass_tot[[2]]$taxo_tot)
    ass1 <- as.matrix(lst_ass_tot[[1]]$ass_tot)
    ass2 <- as.matrix(lst_ass_tot[[2]]$ass_tot)

    ind_FX <- tt1[,2] == 'Fungi_X'

    tt1[ind_FX,] <- tt2[ind_FX,]
    ass1[ind_FX,] <- ass2[ind_FX,]

    taxo_tot <- as.data.frame(tt1)
    ass_tot <- as.data.frame(ass1)

  }

  # name tax_level
  names(taxo_tot) <- switch(i,
                            '01_16S_bact' = c('reign','phylum','class','order','family','genus','species'),
                            '02_18S_euk' = c('reign','phylum','division','class','order','family','genus','species'),
                            '05_ITS_fun' = c('reign','division','class','order','family','genus','species'))
  
  # taxo sort the community ----
  ord_taxo <- order(ass_tot$taxo)

  mr_sort <- mr_tot[,ord_taxo]
  ass_sort <- ass_tot[ord_taxo,]
  taxo_sort <- taxo_tot[ord_taxo,]
  env_sort <- env_tot[row.names(mr_sort),]

  
  # taxo clean communities ----
  taxo_false <- switch(i,
                       '01_16S_bact' = c('Chloroplast','Mitochondria'),
                       '02_18S_euk' = c('Metazoa','Embryophyceae'),
                       '05_ITS_fun' = c('Plantae','Chromista','Animalia','Protista'))

  taxo_false <- paste0(taxo_false, collapse='|')
  ind_tf <- grepl(taxo_false, ass_sort$taxo)
  
  mr_cln <- mr_sort[,!ind_tf]
  mr_cln <- mr_cln[rowSums(mr_cln) != 0,]
  ass_cln <- ass_sort[!ind_tf,]
  taxo_cln <- taxo_sort[!ind_tf,]
  env_cln <- env_sort[row.names(mr_cln),]

  
  # low seq smp (piecewise linear model) ----
  lrs <- log(sort(rowSums(mr_cln)))

  x <- brks <- seq_along(lrs)

  # find the mean square error for each break
  mse <- lh_steep <- NULL
  for(j in brks){
    mod <- lm(lrs~x*(x <= brks[j]) + x*(x < brks[j]))
    mse <- c(mse, summary(mod)$sigma)

    co <- coef(mod)
    lh_steep <- c(lh_steep, co[2]+co[5] > co[2])
  }

  min_mse <- which(mse == min(mse[lh_steep], na.rm=T))

  # building the model according to the minimum mean square error
  mod <- lm(lrs ~ x*(x < min_mse) + x*(x > min_mse))
  co <- coef(mod)

  low_seq <- names(lrs)[1:(min_mse-1)]

  env_cln$low_seq <- row.names(env_cln) %in% low_seq

  #---
  
  X11(width=wdt1, height=wdt1)
  par(mar=c(4,4,4,5))
  plot(lrs~x, xlab='', ylab='log of sequence number', main=i)

  # add the two lm
  curve(co[1]+co[3] + (co[2]+co[5])*x, add=T, from=1, to=min_mse)
  curve(co[1]+co[4] + (co[2])*x, add=T, from=min_mse, to=length(lrs))
  abline(v=min_mse, lty=3)

  # add the curve of mean square error
  mse2 <- (mse-min(mse))/diff(range(mse)) * diff(range(lrs)) + min(lrs)
  lines(1:length(mse2), mse2, col=3)
  
  lgt <- 6
  lab <- seq(min(mse), max(mse), length=lgt)
  at <-(lab-min(mse))/diff(range(mse)) * diff(range(lrs)) + min(lrs) 
  
  axis(4, at, round(lab, 2))
  mtext('mean square error\npiecewise linear model', 4, 3)
  
  #---
  lst_plot[['piece_lin']][[i]] <- recordPlot()
  dev.off()

  
  # communities building ----
  
  # without the low_sequences samples (piecewise regression)
  ind_ls <- env_cln$low_seq == F
  
  mr_nls <- mr_cln[ind_ls,]
  mr_nls <- mr_nls[,colSums(mr_nls) != 0]

  env_nls <- env_cln[ind_ls,]

  ass_nls <- ass_cln[names(mr_nls),]
  taxo_nls <- taxo_cln[names(mr_nls),]

  # centered log ratio on nls
  mr_rcz <- mr_nls[,colSums(decostand(mr_nls, 'pa')) > 1] 
  # take out the OTU found in only one sample otherwise, the replacment of 0 give negative values
  
  mr_rcz <- cmultRepl(mr_rcz, method='CZM', output='p-counts') #30 sec
  mr_clr_nls <- as.data.frame(t(apply(mr_rcz, 1, function(x) log(x) - mean(log(x)) )))

  env_clr_nls <- env_cln[row.names(mr_clr_nls),]

  ass_clr_nls <- ass_tot[names(mr_clr_nls),]
  taxo_clr_nls <- taxo_tot[names(mr_clr_nls),]

  #---
  lst_comm[[i]] <- list(raw = list(env=env_tot[row.names(mr_tot),], mr=mr_tot, ass=ass_tot, taxo=taxo_tot, 
                                   lst_ass_tot=lst_ass_tot, lst_ass_ini=lst_ass_ini),
                        sort= list(env=env_sort, mr=mr_sort, ass=ass_sort, taxo=taxo_sort),
                        cln = list(env=env_cln, mr=mr_cln, ass=ass_cln, taxo=taxo_cln),
                        nls = list(env=env_nls, mr=mr_nls, ass=ass_nls, taxo=taxo_nls),
                        clr_nls = list(env=env_clr_nls, mr=mr_clr_nls, ass=ass_clr_nls, taxo=taxo_clr_nls)
                        )

}

names(lst_comm) <- n_comm


#---
save(env_ini_tot, env_tot, env_unsc, lst_comm, 
     file=paste0(dir_save, '00_lst_comm.Rdata'))

save(wdt1, wdt2, lst_palev, n_comm, depths, wdt1, wdt2, v_selec, leg, lay_ordi, swt,
     file=paste0(dir_save, 'misc.Rdata'))

save(lst_plot, file=paste0(dir_save, 'lst_plot.Rdata'))

save(lst_pca, file=paste0(dir_save, 'lst_pca.Rdata'))

####























