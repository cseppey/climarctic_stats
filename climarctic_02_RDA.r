#%#%#
# climarctic comm vs env
#%#%#

print('##### Climarctic 02 RDA #####')

rm(list=ls())

require(foreach)
require(doSNOW) # e.g. makeSOCKcluster()
require(xtable)

# prep cluster
cl <- makeSOCKcluster(4)

registerDoSNOW(cl)


# DIR LOAD ####
dir_out  <- 'Projets/Climarctic/stats/MBC/out/'
dir_save <- paste0(dir_out, 'saves/') 
dir_rda  <- paste0(dir_out, '02_RDA/')
dir.create(dir_rda, showWarnings=F)

#---
load(paste0(dir_save, '00_lst_comm.Rdata'))
load(paste0(dir_save, 'misc.Rdata'))
load(paste0(dir_save, 'lst_plot.Rdata'))


#---
restart = F

cond <- 'site'

permu <- 10000

# RDA ####

file <- paste0(dir_save, '02_lst_rda2.Rdata')
if(file.exists(file) & restart == F){
  load(file)
} else {

  # loop the communities ---
  
  lst_rda <- NULL
  for(i in n_comm) {
    
    print(paste('####',i,'####'))
      
    mr <- lst_comm[[i]]$clr_nls$mr
    env <- lst_comm[[i]]$clr_nls$env
    
    env <- env[,v_selec]

    # loop the depths ---
    for(j in depths) {
      
      print(paste(i, j, 'model'))
      
      # Note: if many factor included in the condition (interaction or not), 
      # the only factor counting is the one having the highest resolution if hierarchical factors
      
      # rda ----
      e <- env[grepl(j, env$depth),]
      is.fact <- sapply(e, is.factor)
      
      m <- mr[row.names(e),]
      
      f <- formula(paste0(ifelse(j == 'top|deep', 'm~depth+', 'm~'), paste(names(e)[is.fact == F], collapse='+'),
                          '+Condition(', cond, ')', collapse=''))
      
      rda_full <- capscale(f, data=e, scale=T)
      
      lst_rda[[i]][[j]][['full']] <- list(e=e, m=m, f=f, mod=rda_full)
      
      
      # retreive a more parsimonious model ----
      # Note: the parsimonious model did not depend of the order of the variable in the input model
      
      print('ordistep on full model')
      
      file2 <- paste0(dir_save, '02_rda_parsi_', i, '_', j, '.Rdata')
      if(file.exists(file2) & restart == F){
        load(file2)
      } else {
        
        set.seed(0)
        
        f <- formula(paste0(ifelse(j == 'top|deep', 'm~depth+rh+Condition(', 'm~rh+Condition('), cond, ')'))
        
        rda_parsi <- ordistep(capscale(f, data=e, scale=T), formula(rda_full), trace=F)
        
        save(rda_parsi, file=file2)
      }
      
      v_parsi <- attributes(rda_parsi$terminfo$terms)$term.labels
      
      e <- env[grepl(j, env$depth),v_parsi]
      
      m <- mr[row.names(e),]
      m <- m[,colSums(m) != 0]
      
      f <- formula(paste(c(ifelse(j == 'top|deep', 'm~depth+rh', 'm~rh'),
                           v_parsi[grepl('depth|rh|site', v_parsi) == F], 
                           paste('Condition(', cond, ')')), collapse='+'))
      
      rda_parsi <- capscale(f, data=e, scale=T)
    
      lst_rda[[i]][[j]][['parsi']] <- list(e=e, m=m, f=f, mod=rda_parsi)
      
      
      # retreive info on the two models ----
      
      mod_type <- names(lst_rda[[1]][[1]])
      
      # lst_ordi <- NULL
      for(k in mod_type){
        
        print(paste('tests on', k))
        
        mod <- lst_rda[[i]][[j]][[k]]$mod

        v <- rev(rev(attributes(mod$terminfo$terms)$term.labels)[-1])
        
        # calculate the variance of the variables
        stat <- foreach(l=v) %dopar% {
        
          m   <- lst_rda[[i]][[j]][[k]]$m
          e   <- lst_rda[[i]][[j]][[k]]$e
          f   <- lst_rda[[i]][[j]][[k]]$f
          
          mod <- capscale(eval(parse(text=paste(as.character(f)[c(2,1,3)], collapse=''))),
                          data=e, scale=T) # some fuck with the foreach environment
                                           # but output are the same then if tested sequencially
          
          set.seed(0)
          
          # as the only variable of the model
          rda1 <- capscale(as.formula(paste('m~', l, '+Condition(', cond, ')')), data=e, scale=T)
          pv1 <- signif(anova(rda1, permutations=permu)$`Pr(>F)`[1], 2)
          va1 <- RsquareAdj(rda1)[[1]]
          
          # when taken out of the full model
          upd <- update(mod, as.formula(paste('.~.-', l)))
          pv2 <- signif(anova(mod, upd, permutations=permu)$`Pr(>F)`[2], 2)
          va2 <- RsquareAdj(mod)[[1]] - RsquareAdj(upd)[[1]] # no R2 adj because Conditon(site)
          
          info <- list(pv_uniq=pv1, va_uniq=va1, pv_part=pv2, va_part=va2)
          info[as.logical(!sapply(info, length))] <- NA
          
          return(unlist(info))
        }
            
        #---
        stat <- t(data.frame(stat))
        row.names(stat) <- v
        
        # retreive the RDA informations ---
        s <- summary(mod)
        
        n_fact <- row.names(s$biplot)[row.names(s$biplot) %in% names(is.fact)[is.fact == F]]
        var <- s$biplot[n_fact,1:2]
        if(is.matrix(var) == F){
          var <- matrix(var, ncol=2)
          dimnames(var) <- list(n_fact, c('CAP1','CAP2'))
        }
        
        lst_rda[[i]][[j]][[k]] <- append(lst_rda[[i]][[j]][[k]], list(smp        = s$site[,1:2],
                                                                      var        = var,
                                                                      axes_names = paste('RDA; var =', 
                                                                                         signif(s$cont$importance[2,1:2], 2)),
                                                                      stat       = stat))
        
      }
    }
  }
  
  save(lst_rda, file=file)

}


# GRAF ####

for(i in n_comm){
  
  X11(width=wdt2, height=wdt2*1.2)
  lay_ordi(lst_rda[[i]], x2='parsi', env=env_tot)
  
  lst_plot[['RDA']][[i]] <- recordPlot()
  dev.off()
  
}

save(lst_plot, file=paste0(dir_save, 'lst_plot.Rdata'))

save(lst_rda, file=paste0(dir_save, 'lst_rda.Rdata'))



####




















