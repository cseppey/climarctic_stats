#%#%#
# Rpart
#%#%#

print('##### Climarctic 06 Rpart #####')

rm(list=ls())

require(foreach)
require(doSNOW) # e.g. makeSOCKcluster()
require(rpart.utils) # rpart.subrules.table, rpart.rules.table
require(rpart.plot) # prp
require(mvpart)
require(labdsv)
require(plotrix)

source('~/bin/src/my_prog/R/pie_taxo.r')
source('~/bin/src/my_prog/R/pie_taxo_single.r')
source('~/bin/src/my_prog/R/legend_pie_taxo.r')
source('~/bin/src/my_prog/R/Rpart_Indval.r')


# prep cluster
cl <- makeSOCKcluster(4)

clusterEvalQ(cl, library(labdsv))

registerDoSNOW(cl)

#---
restart <- F

# DIR LOAD ####
dir_out   <- 'Projets/Climarctic/stats/MBC/out/'
dir_save  <- paste0(dir_out, 'saves/') 
dir_rpart <- paste0(dir_out, '06_Rpart/')
dir.create(dir_rpart, showWarnings=F)

#---
load(paste0(dir_save, '00_lst_comm.Rdata'))
load(paste0(dir_save, 'misc.Rdata'))
load(paste0(dir_save, 'lst_plot.Rdata'))


# MRT + INDVAL ####

file <- paste0(dir_save, '06_lst_mrt.Rdata')
if(file.exists(file) & restart == F){
  load(file)
} else {
  
  lst_mrt <- NULL
  for(i in n_comm){
    
    print(paste('####',i,'####'))
    
    # retreive info comunity
    lst <- lst_comm[[i]]$clr_nls
    
    # env <- lst$env
    mr <- lst$mr
    ass <- lst$ass
    taxo <- lst$taxo
    
    # select variables and tax levels
    e_rpart <- env_unsc[row.names(mr),v_selec[-c(1:2)]]
    t_rpart <- taxo[,2:5]  
    
    # calculate the MRT
    system.time(lst_mrt[[i]] <- mrt_indval(comm=mr, taxo=t_rpart, env=e_rpart, dist = 'euc', maxdepth = 3,
                                           dom = 0, p_tresh = 0.75, iter = 10000, prop_thresh=0.05, root=swt(i)))  
    
  }
  
  save(lst_mrt, file=file)
  
}

# plot
for(i in n_comm){
  
  X11(width=wdt2, height=wdt2*1.2)
  mrt_indval(tree_plot=lst_mrt[[i]])  
  
  lst_plot[['MRT']][[i]] <- recordPlot()
  dev.off()
  
}

save(lst_plot, wdt1, wdt2, file=paste0(dir_save, 'lst_plot.Rdata'))



###






















