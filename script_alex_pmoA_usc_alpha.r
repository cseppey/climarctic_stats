



dir_in <- 'Projets/Climarctic/bioinfo/archive/191229/'
dirs <- c(dir_mb661 <- paste0(dir_in, '03_pmoA_mb661/'),
          dir_A682  <- paste0(dir_in, '04_pmoA_A682/'))

comm <- c('mb661','A682')

lst_data <- NULL
for(i in comm){
  
  dir <- grep(i, dirs, value=T)
  mr <-  read.table(paste0(dir,list.files(dir, '.mr')))
  ass <- read.table(paste0(dir,list.files(dir, '.ass')), sep='\t')
  fa <-  read.table(paste0(dir,list.files(dir, '.fa')))
  
  tax <- ass$V2
  names(tax) <- ass$V1
  tax <- tax[names(mr)]
  tax <- gsub('[[:punct:]][[:digit:]]{2,3}[[:punct:]]{2}|;', ';', tax)
  
  ass <- data.frame(tax, row.names=names(mr))
  
  taxo <- matrix(unlist(strsplit(as.character(ass$tax), ';', fixed=T)), ncol=8, byrow=T)
  row.names(taxo) <- names(mr)
    
  fa_n <- as.character(fa$V1[seq(1, nrow(fa), by=2)])
  fa_n <- substr(fa_n, 2, nchar(fa_n))
  fa <- data.frame(seq=fa$V1[seq(2, nrow(fa), by=2)])
  row.names(fa) <- fa_n
  
  lst_data[[i]] <- list(mr=mr, taxo=taxo, fa=fa)
  
  #---
  ind_alpha <- taxo[,8] == 'USC-alpha'

  file=paste0('Projets/Climarctic/stats/MBC/USC_alpha_', i, '.fa')
  if(file.exists(file)){
    file.remove(file)
  }
  for(j in which(ind_alpha)){
    write.table(fa_n[j], file, T, F, row.names=F, col.names=F)
    write.table(fa$seq[j], file, T, F, row.names=F, col.names=F)
  }
  # 
  
}

